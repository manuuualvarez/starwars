//
//  HomeRouting.swift
//  Starwars
//
//  Created by Manuel on 31/01/2020.
//  Copyright © 2020 DeveloperMA.com. All rights reserved.
//

import Foundation
import UIKit


protocol HomeRouting {
    
}

class HomeRouter {
    
    var view: UIViewController
    
    init(view: UIViewController) {
        self.view = view
    }
}

extension HomeRouter: HomeRouting{
    
}
