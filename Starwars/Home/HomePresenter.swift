//
//  HomePresenter.swift
//  Starwars
//
//  Created by Manuel on 31/01/2020.
//  Copyright © 2020 DeveloperMA.com. All rights reserved.
//

import Foundation

protocol HomePresentation {
    func viewDidLoad()
}

class HomePresenter {
    weak var view: HomeView?
    var iteractor: HomeUseCase
    var router: HomeRouting
    
    init(view: HomeView, iteractor:  HomeUseCase, router: HomeRouting) {
        self.view = view
        self.iteractor = iteractor
        self.router = router
    }
}

extension HomePresenter: HomePresentation{
    func viewDidLoad() {
        
    }
    
}
