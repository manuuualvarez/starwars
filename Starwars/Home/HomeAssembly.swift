//
//  HomeAssembly.swift
//  Starwars
//
//  Created by Manuel on 31/01/2020.
//  Copyright © 2020 DeveloperMA.com. All rights reserved.
//

import Foundation
import UIKit


class HomeAssembly{
    static func build () -> UIViewController{
        let storyboard = UIStoryboard.init(name: "Main", bundle: .main)
        let view = storyboard.instantiateViewController(identifier: "HomeViewController") as! HomeViewController
        let iteractor = HomeIteractor()
        let router = HomeRouter(view: view)
        
        let presenter = HomePresenter(view: view, iteractor: iteractor, router: router)
        view.presenter = presenter
        
        view.view.backgroundColor = UIColor.darkGray
        
        
        return view
    }
}
