//
//  ViewController.swift
//  Starwars
//
//  Created by Manuel on 31/01/2020.
//  Copyright © 2020 DeveloperMA.com. All rights reserved.
//

import UIKit

protocol HomeView: class {
    
}

class HomeViewController: UIViewController {

    var presenter: HomePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

extension HomeViewController: HomeView{
    
}

